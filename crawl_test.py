import unittest
from crawl_bbc import CrawlBBC

class crawlbbctester(unittest.TestCase):
    def setUp(self):
        self.crwl = CrawlBBC()
    
    def news_title_fetch_test(self):
        result = self.crwl.title_fetch('http://www.bbc.com/news/magazine-26356099')
        assertEqual(result, 'How many healthy animals do zoos put down?')
