import urllib
import html5lib
from bs4 import BeautifulSoup
class CrawlBBC(object):
    
    def title_fetch(self, url):
        f = urllib.urlopen(url)
        soup = BeautifulSoup(f)
        r = str(soup.title.string).partition(' - ')
        return r[2]
